document.addEventListener("DOMContentLoaded", () => {
  const pays = document.getElementById("pays");
  const SearchFrom = document.getElementById("from");
  const SearchTo = document.getElementById("to");
  const SearchGo = document.getElementById("go");
 
   
  let requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  // on crée un tableau vide devant recueillir les noms de chaque de pays de l'API 
  countriesData=[]
  // requete Pour recuperer les slugs/pays de l'API
  fetch("https://api.covid19api.com/countries", requestOptions)
    .then(response => response.json())
      .then(
        (countryData) =>{
          countryData.forEach((countries) =>
          {
           countriesData.push(countries.Slug); 
          });
          // creation du select
          let selectList = document.createElement("select");
                selectList.id = "mySelect";
                pays.appendChild(selectList);
          
          let typeList = document.createElement("select");
              typeList.id = "selectCase";
              pays.appendChild(typeList);

           //Creation des options (value + text)
          for (let i = 0; i < countriesData.length; i++)
          {
            let option = document.createElement("option");
            option.setAttribute("value", countriesData[i]);
            option.text = countriesData[i];
            selectList.appendChild(option);
          }
        }) 


        
	
        



  SearchGo.addEventListener('click', () => {
    let selectCountry= document.getElementById("mySelect");
    
    fetch(`https://api.covid19api.com/country/${selectCountry.value}?from=${SearchFrom.value}&to=${SearchTo.value}`)
      .then(response=>response.json())
      .then((result) =>{
       
        let deathResult = [];
        let confirmedResult = [];
        let recoveredResult = [];
        let dateResult = [];

        result.forEach(
            (results) => {
            dateResult.push(results.Date)
            })
        // on "ecoute", voir si quel checkbox a ete coché :
        let casesResult = document.getElementById('case');
        [...casesResult.children].forEach( (caseResult) => {

          caseResult.addEventListener('click', (event) => {
              
            let case1 = document.getElementById('case1').checked;
            let case2 = document.getElementById('case2').checked;
            let case3 = document.getElementById('case3').checked;
        
            if (case1 === false){
              deathResult = [];
            }else if(case1 === true ){
              result.forEach(
                  (results) => {
                  deathResult.push(results.Deaths);
                  })
            }

            if (case2 === false){
              confirmedResult = [];
            }else if(case2 === true ){
              result.forEach(
                  (results) => {
                  confirmedResult.push(results.Confirmed);
                  })
            }
            if (case3 === false){
              recoveredResult = [];
             
            }else if(case3 === true ){
              result.forEach(
                  (results) => {
                    recoveredResult.push(results.Recovered);
                  })
            }
           
            let ctx = document.getElementById('myChart').getContext('2d');
            let chart = new Chart(ctx, {
             // The type of chart we want to create
             type: 'bar',
             data: {
                 labels: ['Red', 'Blue', 'Yellow'],
                 datasets: [{
                     label: 'Cas confirmes',
                     data: confirmedResult,
                     fillColor: "#79D1CF",
                     strokeColor: "#79D1CF",
                 },{
                     label: 'Death',
                     data: deathResult,
                     type: 'line',
                     backgroundColor: ['red']
                 },{
                   label : 'Gueris',
                   data : recoveredResult,
                   backgroundColor: ['green']
                 }],
                 labels: dateResult,
                 
             },
         
             // Configuration options go here
             options: {}
     });
          })

          });
        
      });
     
  });

  // Affichage des données francasises -> ok
    fetch(
           ` https://coronavirusapi-france.now.sh/FranceLiveGlobalData`
       ).then(
           (response) => {
           response.json().then(
               (covidData) => {
                //   console.log(covidData);
   
                  
                   
                   const date = document.getElementById('date');
                   date.textContent = `Le : ${covidData.FranceGlobalLiveData[0].date}`;
                   
                   const deces = document.getElementById('deces');
                   deces.textContent = ` Nombre de Deces : ${covidData.FranceGlobalLiveData[0].deces}`;
   
                   const hospitalisation = document.getElementById('hospitalisation');
                   hospitalisation.textContent = `nombre d'hospitalisation : ${covidData.FranceGlobalLiveData[0].hospitalises}`;
           });
       });
})
